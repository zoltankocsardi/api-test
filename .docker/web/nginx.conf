
user  nginx;
worker_processes  auto;

error_log /dev/stderr info;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    include       /etc/nginx/fastcgi.conf;
    #include       /etc/nginx/proxy.conf;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /dev/stdout;

    sendfile        on;
    #tcp_nopush     on;

    ##
    # buffer settings
    #
    client_header_buffer_size   1k;
    large_client_header_buffers 4 8k;
    client_body_buffer_size     10K;
    client_max_body_size        8m;

    ##
    # timeout settings
    #
    client_body_timeout     12;
    client_header_timeout   12;
    keepalive_timeout       65;
    send_timeout            10;

    ##
    # gzip settings
    #
    gzip         on;
    gzip_disable "msie6";

    gzip_vary           on;
    gzip_proxied        any;
    gzip_comp_level     4;
    gzip_buffers        16 8k;
    gzip_http_version   1.1;
    gzip_min_length     256;
    gzip_types          text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

    include /etc/nginx/conf.d/*.conf;
}
