<?php
declare(strict_types=1);

namespace Tests;

use ArrayIterator;
use DateTimeImmutable;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Fusion\Common\Application\ApiUser;
use Fusion\Common\Domain\Model\EmailAddress;
use Fusion\Incidents\Domain\Model\Contributor\ContributorId;
use Fusion\Incidents\Domain\Model\Contributor\Moderator;
use Fusion\Incidents\Domain\Model\Display\Colour;
use Fusion\Incidents\Domain\Model\Incident\Incident;
use Fusion\Incidents\Domain\Model\Incident\IncidentId;
use Fusion\Incidents\Domain\Model\Incident\IncidentLocation;
use Fusion\Incidents\Domain\Model\Incident\IncidentReport;
use Fusion\Incidents\Domain\Model\Incident\IncidentTypeId;
use Fusion\Incidents\Domain\Model\Incident\LocationAccuracy;
use Fusion\Incidents\Domain\Model\Map\Geometry\Position;
use Fusion\Incidents\Domain\Model\Map\MapId;
use Fusion\Incidents\Domain\Model\Theme\Theme;
use Fusion\Incidents\Domain\Model\Theme\ThemeDescription;
use Fusion\Incidents\Domain\Model\Theme\ThemeId;
use Fusion\Incidents\Domain\Model\Theme\ThemeName;
use Fusion\Incidents\Domain\Model\Time\CompleteTimeSpan;
use Fusion\Incidents\Domain\Service\IncidentIdentityService;
use Fusion\Incidents\Infrastructure\Service\InMemoryIncidentIdentityService;
use Laravel\Lumen\Application;
use Prooph\Common\Messaging\Message;
use Prooph\EventSourcing\Aggregate\AggregateType;
use Prooph\EventSourcing\AggregateRoot;
use Prooph\EventSourcing\EventStoreIntegration\AggregateTranslator;
use Ramsey\Uuid\Uuid;

/**
 * Base test case
 *
 * @author James Drew <james.drew@growthcapitalventures.co.uk>
 */
abstract class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    /**
     * @var AggregateTranslator
     */
    private $aggregateTranslator;
    /** @var Generator */
    private $generator;

    // General Helpers ----

    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication(): Application
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function setUp()
    {
        parent::setUp();

        app()->singleton(IncidentIdentityService::class, function() {
           return app(InMemoryIncidentIdentityService::class);
        });
    }

    /**
     * Get recorded events from an aggregate
     *
     * @param AggregateRoot $aggregateRoot
     *
     * @return Message[]
     */
    protected function popRecordedEvents(AggregateRoot $aggregateRoot): array
    {
        $aggregateTranslator = $this->getAggregateTranslator();

        return $aggregateTranslator->extractPendingStreamEvents($aggregateRoot);
    }

    /**
     * Use an event stream to reconstitute an aggregate
     *
     * @param string $aggregateRootClass
     * @param array  $events
     *
     * @return AggregateRoot|\object
     */
    protected function reconstituteAggregateFromHistory(string $aggregateRootClass, array $events): AggregateRoot
    {
        $aggregateTranslator = $this->getAggregateTranslator();

        return $aggregateTranslator->reconstituteAggregateFromHistory(
            AggregateType::fromAggregateRootClass($aggregateRootClass),
            new ArrayIterator($events)
        );
    }

    /**
     * Instantiate and get an aggregate translator
     *
     * @return AggregateTranslator
     */
    private function getAggregateTranslator(): AggregateTranslator
    {
        if(null == $this->aggregateTranslator){
            $this->aggregateTranslator = new AggregateTranslator();
        }

        return $this->aggregateTranslator;
    }

    /**
     * Instantiate and get a faker instance
     *
     * @return Generator
     */
    private function getGenerator(): Generator
    {
        if(null == $this->generator){
            $this->generator = Factory::create('en_GB');
        }

        return $this->generator;
    }

    /**
     * Get the gravatar URL for an email address
     *
     * @param string $emailAddress
     *
     * @return string
     */
    private function gravatarUrl(string $emailAddress): string
    {
        $emailAddressClean = trim(strtolower($emailAddress));
        $emailAddressHash  = md5($emailAddressClean);

        return sprintf("https://www.gravatar.com/avatar/%s", $emailAddressHash);
    }

    // Assertions ----

    /**
     * Assert that the number of events recorded by an aggregate root is as expected
     *
     * @param int           $expected
     * @param AggregateRoot $aggregateRoot
     * @param null|string   $message
     */
    public function assertEventCount(int $expected, AggregateRoot $aggregateRoot, ?string $message = ''): void
    {
        $recordedEvents = $this->popRecordedEvents(clone $aggregateRoot);

        self::assertEquals($expected, count($recordedEvents), $message);
    }

    /**
     * Assert that the event recorded at the specified position is of the expected class
     *
     * @param string        $expected
     * @param int           $position
     * @param AggregateRoot $aggregateRoot
     * @param null|string   $message
     */
    public function assertEventInstanceAt(string $expected, int $position, AggregateRoot $aggregateRoot, ?string $message = ''): void
    {
        $recordedEvents = $this->popRecordedEvents(clone $aggregateRoot);
        $event          = $recordedEvents[$position] ?? null;

        self::assertInstanceOf($expected, $event, $message);
    }

    // Instantiation Helpers ----

    /**
     * Make a moderator aggregate using the given component values or defaults
     *
     * @param ContributorId|null $contributorId
     * @param EmailAddress|null  $emailAddress
     * @param string|null        $name
     * @param string|null        $pictureUrl
     *
     * @return Moderator
     * @throws \Exception
     */
    protected function makeModerator(?ContributorId $contributorId = null, ?EmailAddress $emailAddress = null, ?string $name = null, ?string $pictureUrl = null): Moderator
    {
        $generator = $this->getGenerator();

        $contributorId = $contributorId ?: ContributorId::generate();
        $name          = $name ?: "{$generator->firstName} {$generator->lastName}";
        $emailAddress  = $emailAddress ?: $generator->email;
        $pictureUrl    = $pictureUrl ?: $this->gravatarUrl($emailAddress);

        /** @var Moderator $moderator */
        $moderator = Moderator::register($contributorId, $emailAddress, $name, $pictureUrl);

        return $moderator;
    }

    /**
     * Make a location value object using the given component values, or defaults
     *
     * @param float|null  $latitude
     * @param float|null  $longitude
     * @param int|null    $accuracy
     *
     * @return IncidentLocation
     */
    protected function makeLocation(?float $latitude = null, ?float $longitude = null, ?int $accuracy = null): IncidentLocation
    {
        $generator = $this->getGenerator();

        return IncidentLocation::atPosition(
            Position::fromCoordinates($latitude ?: $generator->latitude, $longitude ?: $generator->longitude),
            LocationAccuracy::fromString(strval($accuracy ?: $generator->randomElement([1, 2, 3])))
        );
    }

    /**
     * Make an incident object using the given component values, or defaults
     *
     * @param IncidentId|null        $incidentId
     * @param ContributorId|null     $reporterId
     * @param MapId|null             $mapId
     * @param IncidentTypeId|null    $incidentTypeId
     * @param null|string            $summary
     * @param IncidentLocation|null  $location
     * @param DateTimeImmutable|null $startedAt
     * @param DateTimeImmutable|null $endedAt
     *
     * @return Incident
     * @throws \Exception
     */
    protected function makeIncident(?IncidentId $incidentId = null, ?ContributorId $reporterId = null, ?MapId $mapId = null, ?IncidentTypeId $incidentTypeId = null, ?string $summary = null, ?IncidentLocation $location = null, ?DateTimeImmutable $startedAt = null, ?DateTimeImmutable $endedAt = null): Incident
    {
        $generator = $this->getGenerator();

        $incidentId     = $incidentId ?: IncidentId::fromSequence($generator->numberBetween(1, 999999999));
        $mapId          = $mapId ?: MapId::generate();
        $incidentTypeId = $incidentTypeId ?: IncidentTypeId::generate();
        $reporterId     = $reporterId ?: ContributorId::generate();
        $location       = $location ?: $this->makeLocation();
        $timeSpan       = CompleteTimeSpan::fromDateTimes(
            $startedAt ?: new DateTimeImmutable('-12 hours'),
            $endedAt ?: new DateTimeImmutable('-10 hours')
        );
        $report         = IncidentReport::fromParts(
            $summary ?: $generator->title,
            $generator->sentence
        );

        return Incident::newReport($incidentId, $mapId, $incidentTypeId, $reporterId, $location, $timeSpan, $report);
    }

    /**
     * Make a theme object using the given component values or defaults
     *
     * @param ThemeId|null          $themeId
     * @param ContributorId|null    $analystId
     * @param ThemeName|null        $name
     * @param ThemeDescription|null $description
     * @param Colour|null           $colour
     * @param IncidentId[]          $incidentIds
     *
     * @return Theme
     * @throws \Exception
     */
    protected function makeTheme(?ThemeId $themeId = null, ?ContributorId $analystId = null, ?ThemeName $name = null, ?ThemeDescription $description= null, ?Colour $colour = null, IncidentId ...$incidentIds): Theme
    {
        $generator = $this->getGenerator();

        $themeId   = $themeId ?: ThemeId::fromSequence($generator->numberBetween(1, 999999999));
        $analystId = $analystId ?: ContributorId::generate();
        $name      = $name ?: ThemeName::fromString($generator->title);
        $colour    = $colour ?: Colour::fromString($generator->hexColor);

        if(!empty($incidentIds)){
            $incidents = array_map(function(IncidentId $incidentId){
                return $this->makeIncident($incidentId);
            }, $incidentIds);
        }
        else{
            $incidents = [
                $this->makeIncident(),
                $this->makeIncident(),
                $this->makeIncident(),
            ];
        }

        $theme = Theme::identifyNew($themeId, $analystId, $name, $colour, ...$incidents);

        if($description){
            $theme->describe($description);
        }

        return $theme;
    }

    // User Helpers ----

    /**
     * Prepare a typical user from the "Administrators" group
     *
     * @param string|null $uuid
     *
     * @return ApiUser
     * @throws \Exception
     */
    protected function administrator(?string $uuid = null): ApiUser
    {
        $uuid = Uuid::isValid($uuid)? $uuid: Uuid::uuid4()->toString();

        return new ApiUser([
            'id'           => $uuid,
            'auth0_id'     => 'auth0|' . $uuid,
            'is_person'    => true,
            'permissions'  => [
                "report:incidents",
                "update:incidents",
                "update:own-incidents",
                "remove:own-incidents",
                "search:incidents",
                "view:incidents",
                "update:incident-maps",
                "link:incidents",
                "search:sources",
                "search:source-media",
                "search:individuals",
                "search:groups",
                "identify:themes",
                "update:themes",
                "search:themes",
                "view:themes",
                "search:tags",
                "manage:country-assignments",
                "review:incidents",
                "manage:own-authenticator",
                "update:user-groups",
                "watch:incidents",
                "add:users",
                "update:own-account",
                "update:accounts",
                "update:own-profile",
                "update:profiles",
                "update:own-preferences",
                "view:users",
                "search:users",
                "remove:users",
                "search:geography",
                "view:incident-metrics",
                "view:moderation-metrics",
                "view:user-metrics",
                "view:own-account",
                "remove:incidents"
            ],
            'client_id'    => 'if_test',
            'client_scope' => 'read:users read:incidents',
        ]);
    }

    public function getEventFromDatabase($table, string $aggregateId, $event): ?object
    {
        return $this->app['db']->table('_' . sha1($table))
            ->where('event_name', $event)
            ->where('metadata->_aggregate_id', $aggregateId)
            ->first();
    }
}
