<?php

namespace Tests\Incidents\Unit\Presentation\Http\Controller;

use Fusion\Incidents\Domain\Service\IncidentService;
use Fusion\Incidents\Presentation\Http\Controller\IncidentsController;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

class IncidentsControllerTest extends TestCase
{
    /**
     * @var IncidentService
     */
    private $service;

    /**
     * @var IncidentsController
     */
    private $controller;

    public function setUp()
    {
        $this->service = $this->prophesize(IncidentService::class);
        $this->controller = new IncidentsController(
            $this->service->reveal()
        );
    }

    public function test_get_incident_with_correct_data()
    {
        $uuid = '9079f2d2-04ba-4715-aa4d-3baf71317cb7';
        $attributes = ['incidentId' => $uuid];
        $expectedJsonString = '{"status":200,"data":{"id":"9079f2d2-04ba-4715-aa4d-3baf71317cb7"},"message":"success"}';

        $this->service->show($attributes)
            ->shouldBeCalledOnce()
            ->willReturn(
                [
                    'id' => '9079f2d2-04ba-4715-aa4d-3baf71317cb7',
                ]
            );

        $request = Request::create('/incidents/' . $uuid);
        $request->attributes->add($attributes);

        $response = $this->controller->show($request);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedJsonString, $response->getContent());
    }

    public function test_store_incident_with_correct_data()
    {
        $body = (object) [
            'description' => 'Man caught with explosives',
            'reportedAt' => 1596051163,
            'categoryId' => 'be350de0-1004-4fce-ad17-52ec577d2b49',
            'involvedPartyId' => '4c43ac47-eefb-3d73-a417-8c5f5e07c3ee',
            'position' => (object) [
                'longitude' => -51.591839,
                'latitude' => 12.409182,
            ]
        ];

        $request = Request::create('/incidents', 'POST', [], [], [], [], json_encode($body));

        $response = $this->controller->store($request);
        $responseBody = json_decode($response->getContent());

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('Incident has successfully created', $responseBody->data);
    }
}
