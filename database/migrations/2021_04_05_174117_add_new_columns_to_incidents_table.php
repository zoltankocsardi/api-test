<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incidents', function($table) {
            $table->uuid('category_id')->nullable();
            $table->uuid('involved_party_id')->nullable();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('involved_party_id')->references('id')->on('involved_parties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incidents', function($table) {
            $table->dropForeign('category_id');
            $table->dropForeign('involved_party_id');
            $table->dropColumn('category_id');
            $table->dropColumn('involved_party_id');
        });
    }
}
