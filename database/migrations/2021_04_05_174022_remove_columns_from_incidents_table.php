<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsFromIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incidents', function($table) {
            $table->dropColumn('category_id');
            $table->dropColumn('category_name');
            $table->dropColumn('category_colour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incidents', function($table) {
            $table->string('category_id');
            $table->string('category_name');
            $table->string('category_colour');
        });
    }
}
