<?php

use Faker\Factory;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $rows = [
            [
                'id' => $faker->uuid,
                'name' => 'Terrorism',
                'colour' => '#fff',
            ],
            [
                'id' => $faker->uuid,
                'name' => 'Medical',
                'colour' => '#000',
            ],
            [
                'id' => $faker->uuid,
                'name' => 'Accident',
                'colour' => '#0000ff',
            ],
            [
                'id' => $faker->uuid,
                'name' => 'Natural disaster',
                'colour' => '#ccc',
            ],
            [
                'id' => $faker->uuid,
                'name' => 'Drinking in public',
                'colour' => '#ff0000',
            ],
        ];


        DB::table('categories')->insert($rows);
    }
}
