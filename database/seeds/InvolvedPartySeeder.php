<?php

use Faker\Factory;
use Illuminate\Support\Facades\DB;

class InvolvedPartySeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $rows = [];

        for ($i = 0; $i < 10; $i++) {
            $rows[] = [
                'id' => $faker->uuid,
                'name' => $faker->jobTitle,
                'involvement' => $faker->country,
            ];
        }

        DB::table('involved_parties')->insert($rows);
    }

}
