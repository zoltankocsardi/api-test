<?php

use Fusion\Common\Application\Application;

/** @var Application $app */
$app->routeMiddleware([
    'assert' => \Fusion\Common\Application\Middleware\AssertValidationMiddleware::class,
]);
