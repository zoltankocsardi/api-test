# Principles

The following is a brief list of some of the principles we follow as a development team:

### SOLID Principles
We believe in and actively support the use of the SOLID principles. By adopting these principles we aim to create classes that have a single responsibility. We promote the use of interfaces to provide abstractions.

### Testing
Our codebase is backed by a test suite containing unit and feature tests. Each class we introduce is accompanied by a test. We believe in developing small testable components that can be tested in isolation and mocking classes that are injected via dependency injection where required. 

If we find that a class is hard to test in isolation it is an indication there are opportunities to reevaluate the implementation. 

All applications have side effects therefore we adopt the mentality of testing these side effects as well as the happy path.

### PSR Coding Standards
We adhere to PSR-1, PSR-4 and PSR-12 coding standards to help us create a consistent and readable codebase. 
Clear defined doc blocks are expected. Exceptions are explicitly caught then handled or declared `@throws` within the function docblock. Argument type hints and function return types are required by default. 

### Explicit Development
We actively discourage the use of magic in favour of a clear and explicit development approach. Dependency injection is used over global service locators. If a class cannot be tested we encourage breaking the logic into smaller digestible pieces.

### Validation
Validation is a focal part of our codebase. Input, beit from a user or integration with a third party API, is to be validated for correct structure and shape with the relevant exceptions thrown and error messages relayed back to the consumer when validation rules are not met. 
