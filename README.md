# Intelligence Fusion API Test

## Prologue

Thank you for this exciting test! Although this was my very first time when I met with
CQRS with DDD, I absolutely enjoyed it! I know ES wasn't included in your codebase, but
I tried to do my own research regarding CQRS, DDD, ES or even Prooph. 

Now, I have more questions than I had before :)
Please forgive me, if I missed the ES part or placed certain classes into wrong folders.

## Additional information about setting up the environment

Please, run the migration with seed.

```bash
docker-compose exec application php artisan migrate --seed
```

Also, I exported a postman collection for testing the code easier.
Please find it in the docs/postman folder.

## Remarks

* I implemented a validation middleware using your Assert lib and extended
  the exception handler with a small validation exception
* I implemented a fractal trait for data output. (instead of creating a service
  provider, because I tried to separate the code from Lumen as much as possible)
* I changed the response a bit and added a response trait (for being able to use anywhere,
  for instance in a middleware)
* I created 2 new tables for the categories and the involved parties. Unfortunately I'm
  still unsure which database schema would be the best fit for CQRS/ES. I believe the
  star/snowflake could be a good choice, and I tried to design the DB in that way.
* I also implemented repositories

## If I had more time, I would...

* do way more tests
* add authorization with JWT
* implement a basic React client :)
