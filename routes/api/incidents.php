<?php
declare(strict_types=1);

use Fusion\Common\Application\Factory\AssertValidationFactory;
use Laravel\Lumen\Routing\Router;

/*
|--------------------------------------------------------------------------
| Incidents Context Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the incidents context.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Router $router */
/** @var AssertValidationFactory $factory */
$factory = app(AssertValidationFactory::class);

$router->group(['prefix' => 'incidents'], static function() use ($router, $factory) {
    $router->post('/', $factory->bind(
        \Fusion\Incidents\Presentation\Http\Controller\IncidentsController::STORE,
        \Fusion\Incidents\Domain\Assertion\CreateIncidentAssert::class
    ));

    $router->get('{incidentId}', $factory->bind(
        \Fusion\Incidents\Presentation\Http\Controller\IncidentsController::SHOW,
        \Fusion\Incidents\Domain\Assertion\GetIncidentAssert::class
    ));
});
