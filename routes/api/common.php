<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Common Context Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the common context.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/** @var \Laravel\Lumen\Routing\Router $router */

// Default route
$router->get("/", function(){
    return new \Illuminate\Http\Response('Good luck - we believe in you!!');
});
