<?php

declare(strict_types=1);

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->group(['namespace' => 'Fusion\Common\Http\Controller'], function(Router $router) {
    require __DIR__ . '/api/common.php';
});

require __DIR__ . '/api/incidents.php';
