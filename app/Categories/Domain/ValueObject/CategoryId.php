<?php

declare(strict_types=1);

namespace Fusion\Categories\Domain\ValueObject;

use Fusion\Common\Domain\Model\ValueObject;
use Ramsey\Uuid\Uuid;

final class CategoryId extends ValueObject
{
    /**
     * @var string
     */
    private $idString;

    /**
     * @param string $idString
     */
    private function __construct(string $idString)
    {
        $this->idString = $idString;
    }

    /**
     * @return self
     *
     * @throws \Exception
     */
    public static function generate(): self
    {
        return new self(Uuid::uuid4()->toString());
    }

    /**
     * @param string $idString
     *
     * @return self
     */
    public static function fromString(string $idString): self
    {
        return new self($idString);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->idString;
    }
}
