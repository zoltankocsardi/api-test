<?php

declare(strict_types=1);

namespace Fusion\Categories\Domain\Entity;

use Fusion\Categories\Domain\ValueObject\CategoryId;
use stdClass;

class Category
{
    /**
     * @var CategoryId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $colour;

    /**
     * @param CategoryId $id
     * @param string $name
     * @param string $colour
     */
    private function __construct(CategoryId $id, string $name, string $colour) {
        $this->id = $id;
        $this->name = $name;
        $this->colour = $colour;
    }

    /**
     * @return CategoryId
     */
    public function getId(): CategoryId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getColour(): string
    {
        return $this->colour;
    }

    /**
     * @param CategoryId $id
     * @param stdClass $category
     *
     * @return static
     */
    public static function createFromDatabase(CategoryId $id, stdClass $category): self
    {
        return new self(
            $id,
            $category->name,
            $category->colour
        );
    }
}
