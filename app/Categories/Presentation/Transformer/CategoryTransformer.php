<?php

declare(strict_types=1);

namespace Fusion\Categories\Presentation\Transformer;

use Fusion\Categories\Domain\Entity\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * @param Category $category
     *
     * @return array
     */
    public function transform(Category $category): array
    {
        return [
            'id' => $category->getId()->toString(),
            'name' => $category->getName(),
            'colour' => $category->getColour(),
        ];
    }
}
