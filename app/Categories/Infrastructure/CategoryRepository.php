<?php

declare(strict_types=1);

namespace Fusion\Categories\Infrastructure;

use Fusion\Categories\Domain\Entity\Category;
use Fusion\Categories\Domain\ValueObject\CategoryId;
use Fusion\Common\Application\Exception\NotFoundException;
use Illuminate\Support\Facades\DB;

class CategoryRepository implements CategoryRepositoryInterface
{
    /**
     * @inheritDoc
     *
     * @throws NotFoundException When Category entity is not found
     */
    public function get(CategoryId $id): Category
    {
        $category = DB::table('categories')->find($id->toString());
        if ($category === null) {
            throw new NotFoundException('Incident entity does not exist');
        }

        return Category::createFromDatabase($id, $category);
    }
}
