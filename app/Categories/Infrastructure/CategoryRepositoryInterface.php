<?php

declare(strict_types=1);

namespace Fusion\Categories\Infrastructure;

use Fusion\Categories\Domain\Entity\Category;
use Fusion\Categories\Domain\ValueObject\CategoryId;

interface CategoryRepositoryInterface
{
    /**
     * @param CategoryId $id
     *
     * @return Category
     */
    public function get(CategoryId $id): Category;
}
