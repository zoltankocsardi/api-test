<?php

declare(strict_types=1);

namespace Fusion\InvolvedParties\Domain\Entity;

use Fusion\InvolvedParties\Domain\ValueObject\InvolvedPartyId;
use stdClass;

class InvolvedParty
{
    /**
     * @var InvolvedPartyId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $involvement;

    /**
     * @param InvolvedPartyId $id
     * @param string $name
     * @param string $involvement
     */
    private function __construct(InvolvedPartyId $id, string $name, string $involvement) {
        $this->id = $id;
        $this->name = $name;
        $this->involvement = $involvement;
    }

    /**
     * @return InvolvedPartyId
     */
    public function getId(): InvolvedPartyId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getInvolvement(): string
    {
        return $this->involvement;
    }

    /**
     * @param InvolvedPartyId $id
     * @param stdClass $involvedParty
     *
     * @return static
     */
    public static function createFromDatabase(InvolvedPartyId $id, stdClass $involvedParty): self
    {
        return new self(
            $id,
            $involvedParty->name,
            $involvedParty->involvement
        );
    }
}
