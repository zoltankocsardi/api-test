<?php

declare(strict_types=1);

namespace Fusion\InvolvedParties\Presentation\Transformer;

use Fusion\InvolvedParties\Domain\Entity\InvolvedParty;
use League\Fractal\TransformerAbstract;

class InvolvedPartyTransformer extends TransformerAbstract
{
    /**
     * @param InvolvedParty $involvedParty
     *
     * @return array
     */
    public function transform(InvolvedParty $involvedParty): array
    {
        return [
            'id' => $involvedParty->getId()->toString(),
            'name' => $involvedParty->getName(),
            'involvement' => $involvedParty->getInvolvement(),
        ];
    }
}
