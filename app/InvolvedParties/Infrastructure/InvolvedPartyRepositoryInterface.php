<?php

declare(strict_types=1);

namespace Fusion\InvolvedParties\Infrastructure;

use Fusion\InvolvedParties\Domain\Entity\InvolvedParty;
use Fusion\InvolvedParties\Domain\ValueObject\InvolvedPartyId;

interface InvolvedPartyRepositoryInterface
{
    /**
     * @param InvolvedPartyId $id
     *
     * @return InvolvedParty
     */
    public function get(InvolvedPartyId $id): InvolvedParty;
}
