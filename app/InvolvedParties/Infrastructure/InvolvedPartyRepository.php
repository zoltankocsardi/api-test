<?php

declare(strict_types=1);

namespace Fusion\InvolvedParties\Infrastructure;

use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\InvolvedParties\Domain\Entity\InvolvedParty;
use Fusion\InvolvedParties\Domain\ValueObject\InvolvedPartyId;
use Illuminate\Support\Facades\DB;

class InvolvedPartyRepository implements InvolvedPartyRepositoryInterface
{
    /**
     * @inheritDoc
     *
     * @throws NotFoundException When InvolvedParty entity is not found
     */
    public function get(InvolvedPartyId $id): InvolvedParty
    {
        $involvedParty = DB::table('involved_parties')->find($id->toString());
        if ($involvedParty === null) {
            throw new NotFoundException('Incident entity does not exist');
        }

        return InvolvedParty::createFromDatabase($id, $involvedParty);
    }
}
