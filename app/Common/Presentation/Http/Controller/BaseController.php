<?php

declare(strict_types=1);

namespace Fusion\Common\Presentation\Http\Controller;

use Fusion\Common\Presentation\Http\Traits\ResponseTrait;

abstract class BaseController
{
    use ResponseTrait;
}
