<?php

namespace Fusion\Common\Presentation\Http\Dictionary;

class Response
{
    public const MESSAGE_ERROR = 'error';
    public const MESSAGE_SUCCESS = 'success';
}
