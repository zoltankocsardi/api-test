<?php

namespace Fusion\Common\Presentation\Http\Serializer;

use League\Fractal\Serializer\ArraySerializer;

class FusionSerializer extends ArraySerializer
{
    /**
     * @inheritDoc
     */
    public function collection($resourceKey, array $data): array
    {
        return $resourceKey ? [$resourceKey => $data] : $data;
    }
}
