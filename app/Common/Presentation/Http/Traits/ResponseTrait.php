<?php

declare(strict_types=1);

namespace Fusion\Common\Presentation\Http\Traits;

use Fusion\Common\Presentation\Http\Dictionary\Response;
use Illuminate\Http\JsonResponse;

trait ResponseTrait
{
    /**
     * @param string|array $data
     * @param int $status
     * @param string|null $message
     *
     * @return JsonResponse
     */
    public function generateResponse(
        $data,
        int $status = 0,
        ?string $message = null
    ): JsonResponse {
        $status = $status > 0 ? $status : 500;
        $response = [
            'status' => $status,
            'data' => $data,
            'message' => $message ?? ($status > 320 ? Response::MESSAGE_ERROR : Response::MESSAGE_SUCCESS),
        ];

        return response()->json(
            $response,
            $status,
            [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ]
        );
    }
}
