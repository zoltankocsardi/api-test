<?php

declare(strict_types=1);

namespace Fusion\Common\Presentation\Http\Traits;

use Fusion\Common\Presentation\Http\Serializer\FusionSerializer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\Manager;
use League\Fractal\Scope;
use League\Fractal\TransformerAbstract;

trait FractalTrait
{
    /**
     * @var string
     */
    protected static $collection = 'Collection';

    /**
     * @var string
     */
    protected static $item = 'Item';

    /**
     * Create response with basic fractal methods.
     *
     * If parseInclude is also needed, feel free to extend this method
     *
     * @param Collection|Model|null|\stdClass $model
     * @param TransformerAbstract $transformer
     * @param string $resourceType
     *
     * @return Scope
     */
    protected function createFractal(
        $model,
        TransformerAbstract $transformer,
        string $resourceType
    ): Scope {
        $manager = new Manager();
        $manager->setSerializer(new FusionSerializer());

        $resourceClass = sprintf('League\Fractal\Resource\%s', $resourceType);
        $resource = new $resourceClass($model, $transformer);

        return $manager->createData($resource);
    }
}
