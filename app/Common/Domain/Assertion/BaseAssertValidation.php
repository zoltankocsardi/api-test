<?php

declare(strict_types=1);

namespace Fusion\Common\Domain\Assertion;

use Fusion\Common\Domain\Model\Assert;
use Illuminate\Support\Facades\DB;

abstract class BaseAssertValidation extends Assert
{
    /**
     * Validate the given parameters.
     *
     * @param array $attributes
     */
    abstract public function validate(array $attributes): void;


    /**
     * @param string $table
     * @param string $column
     * @param string $value
     *
     * @return bool
     */
    protected function isValueExistIn(string $table, string $column, string $value): bool
    {
        return DB::table($table)->where($column, $value)->first() === null;
    }
}
