<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Factory;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Presentation\Http\Controller\BaseController;
use UnexpectedValueException;

final class AssertValidationFactory
{
    /**
     * This factory passes the validation class to the middleware.(it expects full namespace name. For now.)
     * @see https://lumen.laravel.com/docs/5.4/middleware
     *
     * @param string $controllerClass
     * @param string $assertClass
     *
     * @return array
     * @throws UnexpectedValueException If the controller or the assert class is not inherited.
     */
    public function bind(string $controllerClass, string $assertClass): array
    {
        $assertObject = app($assertClass);
        $controllerObject = strpos($controllerClass, '@') !== false
            ? app(explode('@', $controllerClass)[0])
            : app($controllerClass);

        if (!$controllerObject instanceof BaseController) {
            throw new UnexpectedValueException(
                sprintf('%s has to be an instance of %s', $controllerClass, BaseController::class)
            );
        }

        if (!$assertObject instanceof Assert) {
            throw new UnexpectedValueException(
                sprintf('%s has to be an instance of %s', $assertClass, Assert::class)
            );
        }

        return [
            'uses' => $controllerClass,
            'middleware' => sprintf('%s:%s', 'assert', $assertClass),
        ];
    }
}
