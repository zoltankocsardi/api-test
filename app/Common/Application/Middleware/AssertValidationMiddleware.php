<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Middleware;

use Closure;
use Fusion\Common\Domain\Assertion\BaseAssertValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use InvalidArgumentException;

class AssertValidationMiddleware
{
    /**
     * Validate the attributes based on the passed Assert class.
     *
     * @param Request $request
     * @param Closure $next
     * @param string $assertName
     *
     * @return JsonResponse
     * @throws InvalidArgumentException When the request has the
     */
    public function handle(Request $request, Closure $next, string $assertName): JsonResponse
    {
        /** @var BaseAssertValidation $assert */
        $assert = app($assertName);
        $comparison = array_intersect_key($request->all(), $request->route()[2]);
        if ($comparison) {
            throw new InvalidArgumentException(
                'Request has at least one parameter, which is in both the query and the payload with the same name'
            );
        }

        $data = array_merge($request->all(), $request->route()[2]);
        $assert->validate($data);

        // since the parameter bag is already in the request, add the validated attributes to it
        $request->attributes->add($data);

        return $next($request);
    }
}
