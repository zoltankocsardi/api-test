<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\Entity;

use DateTimeImmutable;
use Fusion\Categories\Domain\ValueObject\CategoryId;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;
use Fusion\InvolvedParties\Domain\ValueObject\InvolvedPartyId;
use stdClass;

class Incident
{
    /**
     * @var IncidentId
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var DateTimeImmutable
     */
    private $reportedAt;

    /**
     * @var Position
     */
    private $position;

    /**
     * @var CategoryId
     */
    private $categoryId;

    /**
     * @var InvolvedPartyId
     */
    private $involvedPartyId;

    /**
     * @param IncidentId $id
     * @param string $description
     * @param DateTimeImmutable $reportedAt
     * @param Position $position
     * @param CategoryId $categoryId
     * @param InvolvedPartyId $involvedPartyId
     */
    private function __construct(
        IncidentId $id,
        string $description,
        DateTimeImmutable $reportedAt,
        Position $position,
        CategoryId $categoryId,
        InvolvedPartyId $involvedPartyId
    ) {
        $this->id = $id;
        $this->description = $description;
        $this->reportedAt = $reportedAt;
        $this->position = $position;
        $this->categoryId = $categoryId;
        $this->involvedPartyId = $involvedPartyId;
    }

    /**
     * @return IncidentId
     */
    public function getId(): IncidentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getReportedAt(): DateTimeImmutable
    {
        return $this->reportedAt;
    }

    /**
     * @return Position
     */
    public function getPosition(): Position
    {
        return $this->position;
    }

    /**
     * @return CategoryId
     */
    public function getCategoryId(): CategoryId
    {
        return $this->categoryId;
    }

    /**
     * @return InvolvedPartyId
     */
    public function getInvolvedPartyId(): InvolvedPartyId
    {
        return $this->involvedPartyId;
    }

    /**
     * @param IncidentId $id
     * @param stdClass $incident
     *
     * @return self
     */
    public static function createFromDatabase(IncidentId $id, stdClass $incident): self
    {
        return new self(
            $id,
            $incident->description,
            DateTimeImmutable::createFromFormat('U', (string) $incident->reported_at),
            Position::fromString($incident->position),
            CategoryId::fromString($incident->category_id),
            InvolvedPartyId::fromString($incident->involved_party_id)
        );
    }

    /**
     * @param IncidentId $id
     * @param array $data
     *
     * @return self
     */
    public static function createFromRequest(IncidentId $id, array $data): self
    {
        return new self(
            $id,
            $data['description'],
            DateTimeImmutable::createFromFormat('U', (string) $data['reportedAt']),
            Position::fromCoordinates($data['position']['latitude'], $data['position']['longitude']),
            CategoryId::fromString($data['categoryId']),
            InvolvedPartyId::fromString($data['involvedPartyId'])
        );
    }
}
