<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

final class Position extends ValueObject implements Geometry
{
    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    // Setup ----

    /**
     * Get a position at the origin point
     *
     * @return Position
     */
    public static function atOrigin(): Position
    {
        return new self(0.0, 0.0);
    }

    /**
     * Get a new instance of Position from a latitude and longitude
     *
     * @param float $latitude
     * @param float $longitude
     *
     * @return Position
     */
    public static function fromCoordinates(float $latitude, float $longitude): Position
    {
        return new self(
            round($latitude, 6),
            round($longitude, 6)
        );
    }

    /**
     * Parse a string representation of a set of map coordinates into an instance of Position
     *
     * @param string $pointString
     *
     * @return Position
     */
    public static function fromString(string $pointString): Position
    {
        Assert::that($pointString)->regex('/^POINT\s\(.+\)/', 'The line string is not in a valid format');

        preg_match_all('/\([\-\d\.\s]+\)/', $pointString, $pointStringMatches);

        $coordinateString = rtrim(ltrim($pointStringMatches[0][0], '('), ')');
        $coordinates      = explode(" ", $coordinateString);

        return new self(
            (float) $coordinates[1],
            (float) $coordinates[0]
        );
    }

    /**
     * Coordinates constructor.
     *
     * @param float $latitude
     * @param float $longitude
     */
    private function __construct(float $latitude, float $longitude)
    {
        $this->latitude  = $latitude;
        $this->longitude = $longitude;
    }

    // Commands ----

    /**
     * @inheritDoc
     */
    public function withAdjustedCoordinates(array $coordinates): Geometry
    {
        return new self(
            (float) $coordinates[1],
            (float) $coordinates[0]
        );
    }

    // Queries ----

    /**
     * Get the latitude (Y) co-ordinate
     *
     * @return float
     */
    public function latitude(): float
    {
        return $this->latitude;
    }

    /**
     * Get the longitude (X) co-ordinate
     *
     * @return float
     */
    public function longitude(): float
    {
        return $this->longitude;
    }

    /**
     * @inheritDoc
     */
    public function type(): string
    {
        return "Point";
    }

    /**
     * Get the longitude and latitude in an array (in that order)
     *
     * @return float[]
     */
    public function coordinates(): array
    {
        return [$this->longitude, $this->latitude];
    }

    /**
     * Get this point as a GeoJSON string
     *
     * @return string
     */
    public function asGeoJson(): string
    {
        $data = [
            "type"        => "Point",
            "coordinates" => [
                $this->longitude,
                $this->latitude,
            ]
        ];

        return json_encode($data);
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $longitude = number_format($this->longitude, 6);
        $latitude  = number_format($this->latitude, 6);

        return sprintf("POINT (%s %s)", $longitude, $latitude);
    }
}
