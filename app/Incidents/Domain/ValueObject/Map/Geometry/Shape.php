<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

interface Shape extends Geometry
{
    /**
     * Calculate and return the total signed area
     *
     * @return float
     */
    public function area(): float;

    /**
     * Calculate the position of this shape's centroid
     *
     * @return Position
     */
    public function centroid(): Position;
}
