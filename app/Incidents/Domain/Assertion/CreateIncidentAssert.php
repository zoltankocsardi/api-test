<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\Assertion;

use Fusion\Common\Domain\Assertion\BaseAssertValidation;
use Fusion\Common\Domain\Model\Assert;

class CreateIncidentAssert extends BaseAssertValidation
{
    /**
     * @inheritDoc
     */
    public function validate(array $attributes): void
    {
        Assert::thatAll([
            'description',
            'reportedAt',
            'categoryId',
            'involvedPartyId',
            'position',
        ])->inArray(array_keys($attributes));

        Assert::that($attributes['reportedAt'])->integer();

        Assert::that($attributes['categoryId'])->uuid();
        Assert::that($this->isValueExistIn('categories', 'id', $attributes['categoryId']))
            ->false('The Category does not exist.');

        Assert::that($attributes['involvedPartyId'])->uuid();
        Assert::that($this->isValueExistIn('involved_parties', 'id', $attributes['involvedPartyId']))
            ->false('The InvolvedParty does not exist.');

        Assert::that($attributes['position'])->isArray();
        Assert::that($attributes['position']['latitude'])
            ->between(-90, 90, 'Latitude must be between 90 and -90');
        Assert::that($attributes['position']['longitude'])
            ->between(-180, 180, 'Longitude must be between 180 and -180');

        $this->isValueExistIn('categories', 'id', $attributes['categoryId']);
    }
}
