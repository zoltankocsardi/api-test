<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\Assertion;

use Fusion\Common\Domain\Assertion\BaseAssertValidation;
use Fusion\Common\Domain\Model\Assert;

class GetIncidentAssert extends BaseAssertValidation
{
    /**
     * @inheritDoc
     */
    public function validate(array $attributes): void
    {
        Assert::that($attributes['incidentId'])->uuid();
        Assert::that($this->isValueExistIn('incidents', 'id', $attributes['incidentId']))
            ->false('The Incident does not exist.');
    }
}
