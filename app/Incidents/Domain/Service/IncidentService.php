<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\Service;

use Fusion\Common\Application\Query\DispatchesQueries;
use Fusion\Common\Presentation\Http\Traits\FractalTrait;
use Fusion\Incidents\Application\Command\CreateIncidentCommand;
use Fusion\Incidents\Application\Query\GetIncidentQuery;
use Fusion\Incidents\Presentation\Http\Transformer\IncidentTransformer;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;

class IncidentService
{
    use DispatchesQueries, FractalTrait;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @param CommandBus $commandBus
     * @param QueryBus $queryBus
     */
    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    /**
     * @param array $attributes
     *
     * @return array
     * @throws \Exception
     */
    public function show(array $attributes): array
    {
        $query = GetIncidentQuery::byId($attributes['incidentId']);
        $entities = $this->dispatchQuery($query);

        return $this->createFractal(
            array_shift($entities),
            new IncidentTransformer(...$entities),
            self::$item
        )->toArray();
    }

    /**
     * @param array $attributes
     *
     * @return void
     */
    public function store(array $attributes): void
    {
        $command = new CreateIncidentCommand($attributes);
        $this->commandBus->dispatch($command);
    }
}
