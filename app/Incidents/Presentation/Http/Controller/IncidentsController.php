<?php

declare(strict_types=1);

namespace Fusion\Incidents\Presentation\Http\Controller;

use Fusion\Common\Presentation\Http\Controller\BaseController;
use Fusion\Common\Presentation\Http\Traits\ResponseTrait;
use Fusion\Incidents\Domain\Service\IncidentService;
use Fusion\Incidents\Presentation\Http\Dictionary\Incident;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IncidentsController extends BaseController
{
    public const SHOW = self::class . '@show';
    public const STORE = self::class . '@store';

    use ResponseTrait;

    /**
     * @var IncidentService
     */
    private $service;

    /**
     * @param IncidentService $service
     */
    public function __construct(IncidentService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function show(Request $request): JsonResponse
    {
        $response = $this->service->show($request->attributes->all());

        return $this->generateResponse($response, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $this->service->store($request->attributes->all());

        return $this->generateResponse(Incident::INCIDENT_CREATED, Response::HTTP_CREATED);
    }
}
