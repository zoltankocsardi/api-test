<?php

declare(strict_types=1);

namespace Fusion\Incidents\Presentation\Http\Transformer;

use Fusion\Categories\Domain\Entity\Category;
use Fusion\Categories\Presentation\Transformer\CategoryTransformer;
use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\InvolvedParties\Domain\Entity\InvolvedParty;
use Fusion\InvolvedParties\Presentation\Transformer\InvolvedPartyTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class IncidentTransformer extends TransformerAbstract
{
    /**
     * @inheritDoc
     */
    protected $defaultIncludes = [
        'category',
        'involvedParty',
    ];

    /**
     * @var Category
     */
    private $category;

    /**
     * @var InvolvedParty
     */
    private $involvedParty;

    /**
     * @param Category $category
     * @param InvolvedParty $involvedParty
     */
    public function __construct(Category $category, InvolvedParty $involvedParty)
    {
        $this->category = $category;
        $this->involvedParty = $involvedParty;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return InvolvedParty
     */
    public function getInvolvedParty(): InvolvedParty
    {
        return $this->involvedParty;
    }

    /**
     * @param Incident $incident
     *
     * @return array
     */
    public function transform(Incident $incident): array
    {
        return [
            'id' => $incident->getId()->toString(),
            'description' => $incident->getDescription(),
            'reportedAt' => $incident->getReportedAt()->getTimestamp(),
            'position' => [
                'longitude' => $incident->getPosition()->latitude(),
                'latitude' => $incident->getPosition()->longitude(),
            ]
        ];
    }

    /**
     * @return Item
     */
    public function includeCategory(): Item
    {
        return $this->item($this->getCategory(), new CategoryTransformer);
    }

    /**
     * @return Item
     */
    public function includeInvolvedParty(): Item
    {
        return $this->item($this->getInvolvedParty(), new InvolvedPartyTransformer);
    }
}
