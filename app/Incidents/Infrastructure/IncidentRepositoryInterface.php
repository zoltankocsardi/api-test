<?php

declare(strict_types=1);

namespace Fusion\Incidents\Infrastructure;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Domain\ValueObject\IncidentId;

interface IncidentRepositoryInterface
{
    /**
     * @param IncidentId $id
     *
     * @return Incident
     */
    public function get(IncidentId $id): Incident;

    /**
     * @param Incident $incident
     */
    public function save(Incident $incident): void;
}
