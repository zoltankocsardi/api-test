<?php

declare(strict_types=1);

namespace Fusion\Incidents\Infrastructure;

use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Illuminate\Support\Facades\DB;

class IncidentRepository implements IncidentRepositoryInterface
{
    /**
     * @inheritDoc
     *
     * @throws NotFoundException When Incident entity is not found
     */
    public function get(IncidentId $id): Incident
    {
        $incident = DB::table('incidents')->find($id->toString());
        if ($incident === null) {
            throw new NotFoundException('Incident does not exist');
        }

        return Incident::createFromDatabase($id, $incident);
    }

    /**
     * @inheritDoc
     */
    public function save(Incident $incident): void
    {
        DB::table('incidents')->insert([
            'id' => $incident->getId()->toString(),
            'description' => $incident->getDescription(),
            'reported_at' => $incident->getReportedAt()->getTimestamp(),
            'category_id' => $incident->getCategoryId()->toString(),
            'involved_party_id' => $incident->getInvolvedPartyId()->toString(),
            'position' => $incident->getPosition()->toString(),
        ]);
    }
}
