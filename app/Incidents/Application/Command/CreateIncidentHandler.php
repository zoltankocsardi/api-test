<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Infrastructure\IncidentRepository;

final class CreateIncidentHandler
{
    /**
     * @var IncidentRepository
     */
    private $repository;

    /**
     * @param IncidentRepository $repository
     */
    public function __construct(IncidentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CreateIncidentCommand $command
     *
     * @return void
     */
    public function __invoke(CreateIncidentCommand $command): void
    {
        $incident = Incident::createFromRequest($command->getIncidentId(), $command->getData());

        $this->repository->save($incident);
    }
}
