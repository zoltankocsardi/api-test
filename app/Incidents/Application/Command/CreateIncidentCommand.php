<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\ValueObject\IncidentId;

final class CreateIncidentCommand
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var IncidentId
     */
    private $incidentId;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->incidentId = IncidentId::generate();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return IncidentId
     */
    public function getIncidentId(): IncidentId
    {
        return $this->incidentId;
    }
}
