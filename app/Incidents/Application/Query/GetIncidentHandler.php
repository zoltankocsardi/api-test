<?php

namespace Fusion\Incidents\Application\Query;

use Fusion\Categories\Infrastructure\CategoryRepository;
use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\Incidents\Infrastructure\IncidentRepository;
use Fusion\InvolvedParties\Infrastructure\InvolvedPartyRepository;
use React\Promise\Deferred;

final class GetIncidentHandler
{
    /**
     * @var IncidentRepository
     */
    private $incidentRepository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var InvolvedPartyRepository
     */
    private $involvedPartyRepository;

    /**
     * @param IncidentRepository $incidentRepository
     * @param CategoryRepository $categoryRepository
     * @param InvolvedPartyRepository $involvedPartyRepository
     */
    public function __construct(
        IncidentRepository $incidentRepository,
        CategoryRepository $categoryRepository,
        InvolvedPartyRepository $involvedPartyRepository
    ) {
        $this->incidentRepository = $incidentRepository;
        $this->categoryRepository = $categoryRepository;
        $this->involvedPartyRepository = $involvedPartyRepository;
    }

    /**
     * @param GetIncidentQuery $query
     * @param Deferred $promise
     *
     * @throws NotFoundException
     */
    public function __invoke(GetIncidentQuery $query, Deferred $promise): void
    {
        $incident = $this->incidentRepository->get($query->getIncidentId());
        $category = $this->categoryRepository->get($incident->getCategoryId());
        $involvedParty = $this->involvedPartyRepository->get($incident->getInvolvedPartyId());

        $promise->resolve([$incident, $category, $involvedParty]);
    }
}
