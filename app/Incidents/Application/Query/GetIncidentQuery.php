<?php

namespace Fusion\Incidents\Application\Query;

use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;

final class GetIncidentQuery extends Query
{
    use PayloadTrait;

    /**
     * @var string
     */
    private $id;

    /**
     * @param $id
     *
     * @return self
     */
    public static function byId($id): self
    {
        $id = IncidentId::fromString($id);

        return new self(
            compact(
                'id'
            )
        );
    }

    /**
     * @return IncidentId
     */
    public function getIncidentId(): IncidentId
    {
        return $this->payload['id'];
    }
}
