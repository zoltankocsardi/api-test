<?php

namespace Fusion\Incidents\Application\Exception;

use Illuminate\Auth\Access\AuthorizationException;

class RestrictionException extends AuthorizationException
{
    public static function countryRestricted($allowed = 'allowed countries')
    {
        return new static("Your subscription is restricted to incidents in countries '{$allowed}' only");
    }
}
